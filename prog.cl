/*
 * todo:
 * -other geometry objects structs/unions infrastructure
 *
 * -axis aligned boxes / bounding boxes in geom object
 * -planes, xplane, yplane, zplane, arbitrary plane?
 * -geometry structure/kd-tree  BVH!!!
 *
 * -adjust focal length and focal blur settings with mouse wheel
 * -pass prev minDistance to bounding intersect test
 * -create soft shadows by generating a list of hits or just distances and returning them and use old tricks to dimish edges of  *shadows.  At this point I have to get all of the distances anyway, but with a scene tree structure it might be a lot slower.  Maybe  *only continue if it is in the dimished part of the shadow.
 *
 *
 * /-shadow rays
 * /-multiple averaged primary rays per pixel
 * /-random jittering of primary rays
 *
 * -blinn instead of phong (faster[no pow] and more accurate)
 *
 * /half-seperate normal/point of inter from shading code
 *
 * /-camera controls in java code
 * -geometry creation in java code / collision/clipping
 *
 *
 * -visuals of lightsources
 *
 * -arealights
 *
 * -CSG intersection, union, merge, difference, blob/blend?
 *
 *double mode is broken due to fast_ and native_ methods only taking floats
 *
 *create complex shapes by scaling in one dimension or mabe only scale on the top half or have a scale for the top and bottom and  *smoothstep between them
 *maybe just pass a full matrix for each object and do not set any positions or radius, just intersect with the unit sphere and  *transform the ray/results by the matrix
 *
 *on intersect estimate the coverage of the pixel by checking the angle between the rayDir and a ray created to "an edge"
 *    (like the closest side of the sphere) compared to the angle between the rayDir and a rayDir at the edge of the pixel
 *    instead of the center.  Cast another ray where there should be no coverage by that object.
 *    Determine intersects using distance from ray to bounding sphere.pos so we do not just miss some objects with the chosen rayDir.
 *    When dist <= sphere.radius + (ray radius at the point where the sphere is closest to the ray), evaluate like a cone
 *    This is especially good for soft shadows, bacause use can use the pixel coverage estimate to shade, and find a bigger object //  * behind a smaller one.
 *vsync
 *transparent objects
 *ray structs
 *am I rotating around the wrong point.  Pass lookDir in from Java as well.
 */

#ifdef DOUBLE_FP

    #ifdef AMD_FP
        #pragma OPENCL EXTENSION cl_amd_fp64 : enable
    #else
        #pragma OPENCL EXTENSION cl_khr_fp64 : enable
    #endif

    #define native_cos(N) native_cos((float)N)
    #define native_sin(N) native_sin((float)N)
    #define native_sqrt(N) native_sqrt((float)N)
    #define native_powr(N,M) native_powr((float)N, (float)M)

    #define fast_normalize(N) convert_double4(fast_normalize(convert_float4(N)))
    #define fast_length(N) convert_double4(fast_length(convert_float4(N)))

    #define varf double
    #define varf4 double4
    #define _255 255.0
    #define EPSILON 0.0015
#else
    #define varf float
    #define varf4 float4
    #define _255 255.0f
    #define EPSILON 0.0015f
#endif

#ifdef USE_TEXTURE
    #define OUTPUT_TYPE __write_only image2d_t
#else
    #define OUTPUT_TYPE global uint *
#endif

#define CAM_WIDTH 0.1f

/*
enum RayType{
    PrimaryRay = 0,
    ShadowRay = 1,
    ReflectionRay = 2,
    RefractedRay = 3
};

typedef struct Ray{
    varf4 pos;
    varf4 dir;
} Ray;

typedef struct RayTreeNode{
    Ray ray;
    enum RayType type;
    Intersect intersect;
    struct RayTreeNode children[8]; //con
    int numChildren;
} RayTreeNode;
*/

typedef struct Material{
    varf4 color;
    varf ambientFactor;
    varf diffuseFactor;
    varf phongFactor;
    varf phongSize;
    varf reflectivity;
    varf roughness;    //randomizes the normals
} Material;

typedef struct AABB{
    varf4 lowerPos;
    varf4 upperPos;
} AABB;

typedef struct Sphere{
    varf4 pos;
    varf radius;
    Material material;
    AABB aabb;
    varf4 scaling;
    varf4 inverseScaling;
    bool useTransform;
} Sphere;

typedef struct AxisAlignedBox{
    varf4 lowerPos;
    varf4 upperPos;
    Material material;
} AxisAlignedBox;

typedef struct PointLight{
    varf4 pos;
    varf4 color;
} PointLight;

typedef struct Scene{
    PointLight lights[2];
    int numLights;
    Sphere spheres[5];
    int numSpheres;
    AxisAlignedBox axisAlignedBoxes[2];
    int numAxisAlignedBoxes;
} Scene;

typedef struct DistAndNorm{
    varf distance;
    varf4 interNorm;
} DistAndNorm;


typedef struct Hit{
    varf distance;
    varf4 interPos;
    varf4 interNorm;
    Material material;
    int sphereNum;
    int axisAlignedBoxNum;
} Hit;


constant Scene scene = 
{
    .lights={
        {
            .pos = (varf4)(0.0f,1.0f,0.0f, 0.0f),
            .color = (varf4)(1.0f,1.0f,1.0f,1.0f)
        },
        {
            .pos = (varf4)(2.0f,4.0f,-6.0f, 0.0f),
            .color = (varf4)(1.0f,1.0f,1.0f,1.0f)
        }
    },
    .numLights = 2,
    .spheres = {
        {
            .pos = (varf4)(0.0f,0.0f,16.0f, 0.0f),
            .radius = 10.0f,
            .material = {
                .color = (varf4)(0.3f,0.5f,0.8f, 1.0f),
                .ambientFactor = 0.05f,
                .diffuseFactor = 0.5f,
                .phongFactor = 0.8f,
                .phongSize = 30.0f,
                .reflectivity = 1.0f,
                .roughness = 0.00f
            },
            .aabb = {
                .lowerPos = (varf4)(0.0f-10.0f,0.0f-10.0f,16.0f-10.0f, 0.0f),
                .upperPos = (varf4)(0.0f+10.0f,0.0f+10.0f,16.0f+10.0f, 0.0f)
            }
        },
        {
            .pos = (varf4)(0.0f, 0.0f, 2.0f,  0.0f),
            .radius = 1.0f,
            .material = {
                .color = (varf4)(0.3f,0.3f,0.3f, 1.0f),
                .ambientFactor = 0.05f,
                .diffuseFactor = 0.6f,
                .phongFactor = 0.3f,
                .phongSize = 30.0f,
                .reflectivity = 0.5f,
                .roughness = 0.08f
            },
            .aabb = {
                .lowerPos = (varf4)(0.0f-1.0f, 0.0f-1.0f, 2.0f-1.0f,  0.0f),
                .upperPos = (varf4)(0.0f+1.0f, 0.0f+1.0f, 2.0f+1.0f,  0.0f)
            }
        },
        {
            .pos = (varf4)(-3.5f, 0.0f, 1.0f,  0.0f),
            .radius = 2.0f,
            .material = {
                .color = (varf4)(0.6f,0.2f,0.2f, 1.0f),
                .ambientFactor = 0.05f,
                .diffuseFactor = 0.7f,
                .phongFactor = 0.05f,
                .phongSize = 30.0f,
                .reflectivity = 0.2f,
                .roughness = 0.12f
            },
            .aabb = {
                .lowerPos = (varf4)(-3.5f-2.0f, 0.0f-2.0f, 1.0f-2.0f,  0.0f),
                .upperPos = (varf4)(-3.5f+2.0f, 0.0f+2.0f, 1.0f+2.0f,  0.0f)
            }
        },
        {
            .pos = (varf4)(1.0f, 1.0f, 1.0f,  0.0f),
            .radius = 0.5f,
            .material = {
                .color = (varf4)(0.7f,0.7f,0.1f, 1.0f),
                .ambientFactor = 0.05f,
                .diffuseFactor = 0.5f,
                .phongFactor = 0.8f,
                .phongSize = 30.0f,
                .reflectivity = 0.9f,
                .roughness = 0.00f
            },
            .aabb = {
                .lowerPos = (varf4)(1.0f-0.5f, 1.0f-0.5f, 1.0f-0.5f, 0.0f),
                .upperPos = (varf4)(1.0f+0.5f, 1.0f+0.5f, 1.0f+0.5f, 0.0f)
            }
        },
        {
            .pos = (varf4)(1.0f,1.0f,-1.5f, 0.0f),
            .radius = 0.5f,
            .material = {
                .color = (varf4)(1.0f,1.0f,1.0f, 1.0f),
                .ambientFactor = 0.05f,
                .diffuseFactor = 0.5f,
                .phongFactor = 0.8f,
                .phongSize = 30.0f,
                .reflectivity = 1.0f,
                .roughness = 0.00f
            },
            .aabb = {
                .lowerPos = (varf4)(1.0f-0.5f,1.0f-0.5f,-1.5f-0.5f, 0.0f),
                .upperPos = (varf4)(1.0f+0.5f,1.0f+0.5f,-1.5f+0.5f, 0.0f)
            }
        }
    },
    .numSpheres = 5,
    .axisAlignedBoxes = {
        {
            .lowerPos = (varf4)(-10.0f,-1.1f,-1000.0f, 0.0f),
            .upperPos = (varf4)(-1.0f,-1.0f,2.0f, 0.0f),
            .material = {
                .color = (varf4)(0.3f,0.3f,0.3f, 1.0f),
                .ambientFactor = 0.05f,
                .diffuseFactor = 0.6f,
                .phongFactor = 0.3f,
                .phongSize = 30.0f,
                .reflectivity = 0.5f,
                .roughness = 0.08f
            }
        },
        {
            .lowerPos = (varf4)(-1.0f,-2.0f,-1000.0f, 0.0f),
            .upperPos = (varf4)(100.0f,-1.0f,1.0f, 0.0f),
            .material = {
                .color = (varf4)(0.9f,0.9f,0.9f, 1.0f),
                .ambientFactor = 0.05f,
                .diffuseFactor = 0.5f,
                .phongFactor = 0.8f,
                .phongSize = 30.0f,
                .reflectivity = 0.9f,
                .roughness = 0.0f
            }
        }
    },
    .numAxisAlignedBoxes = 2
};


varf rand(varf r){
    varf discard;
    return fract ( r * r * 10246325.365253525237310, &discard ) ;
}

/* use the SQ version below if possible
//distance between the first point and the line formed by the other two points,
//the answer is for the full line not limited to the line segment
varf pointLineDist(varf4 point, varf4 pointOnLine1, varf4 pointOnLine2){
    return fast_length( cross( point - pointOnLine1, point - pointOnLine2 ) ) / fast_length( pointOnLine2 - pointOnLine1 );   //check for divide by zero?
}
*/

//distance between the first point and the line formed by the other two points,
//the answer is for the full line not limited to the line segment
varf pointLineDistSQ(varf4 point, varf4 pointOnLine1, varf4 pointOnLine2){
    varf4 c = cross( point - pointOnLine1, point - pointOnLine2 );
    varf4 d = pointOnLine2 - pointOnLine1;
    varf lengthcSQ = dot(c,c);
    varf lengthdSQ = dot(d,d);
    return lengthcSQ / lengthdSQ;                     //check for divide by zero?
}

bool canIntersectAABB(varf4 rayPos, varf4 rayDir, AABB aabb){
    varf4 rayDirInv = 1.0f/rayDir;                              //PRECOMPUTE in a ray struct?     //check for divide by zero?

    varf tx1 = (aabb.lowerPos.x-rayPos.x)*rayDirInv.x;
    varf tx2 = (aabb.upperPos.x-rayPos.x)*rayDirInv.x;

    varf tmin = min(tx1, tx2);
    varf tmax = max(tx1, tx2);

    varf ty1 = (aabb.lowerPos.y - rayPos.y)*rayDirInv.y;
    varf ty2 = (aabb.upperPos.y - rayPos.y)*rayDirInv.y;

    tmin = max(tmin, min(ty1, ty2));
    tmax = min(tmax, max(ty1, ty2));

    return tmax >= max(tmin,(varf)0.0f);
}

bool canIntersectAxisAlignedBox(varf4 rayPos, varf4 rayDir, AxisAlignedBox aab){
    varf4 rayDirInv = 1.0f/rayDir;                              //PRECOMPUTE in a ray struct?    //check for divide by zero?

    varf tx1 = (aab.lowerPos.x-rayPos.x)*rayDirInv.x;
    varf tx2 = (aab.upperPos.x-rayPos.x)*rayDirInv.x;

    varf tmin = min(tx1, tx2);
    varf tmax = max(tx1, tx2);

    varf ty1 = (aab.lowerPos.y - rayPos.y)*rayDirInv.y;
    varf ty2 = (aab.upperPos.y - rayPos.y)*rayDirInv.y;

    tmin = max(tmin, min(ty1, ty2));
    tmax = min(tmax, max(ty1, ty2));

    return tmax >= max(tmin,(varf)0.0f);
}


varf intersectSphere(varf4 worldRayPos, varf4 worldRayDir, Sphere sphere){
    if(!canIntersectAABB(worldRayPos, worldRayDir, sphere.aabb)) return NAN;

    varf4 rayPos = worldRayPos;
    varf4 rayDir = worldRayDir;

    rayPos -= sphere.pos;

    if(sphere.useTransform){
        rayPos *= sphere.inverseScaling;
        rayDir *= sphere.inverseScaling;
        rayDir = fast_normalize(rayDir);
    }

    varf4 dirToSphere = -rayPos;
    varf distanceToSphereSQ = dot(dirToSphere,dirToSphere);
    varf rSQ = sphere.radius*sphere.radius;

    if(distanceToSphereSQ < rSQ){
        //the ray started inside the sphere, return no intersect or reverse normals(in wrong method for that)
        return NAN;
    }

    varf testA = dot(dirToSphere, rayDir);

    if(testA >= 0.0f){               //sphere is behind rayOriginPos
        varf4 projOfSphereOntoRay = dot(rayDir, dirToSphere)*rayDir+rayPos;
        varf4 toProj = -projOfSphereOntoRay;
        varf distanceToProjSQ = dot(toProj,toProj);

        if(distanceToProjSQ <= rSQ){
            varf4 t = projOfSphereOntoRay;
            varf lengthtSQ = dot(t,t);
            varf distFromProjCenterToInter1SQ = rSQ - lengthtSQ;

            if(distFromProjCenterToInter1SQ <= rSQ){    //rayPos is outside sphere
                varf dist = fast_length(projOfSphereOntoRay - rayPos) - native_sqrt(distFromProjCenterToInter1SQ);    //can I get rid of this, return SQ?

                if(sphere.useTransform){
                    varf4 interPos = rayPos + (dist*rayDir);
                    varf4 worldInterPos = interPos * sphere.scaling + sphere.pos;
                    return fast_length(worldRayPos-worldInterPos);
                }
                else{
                    return dist;
                }

            }

        }

    }

    return NAN;
}


bool canIntersectSphere(varf4 rayPos, varf4 rayDir, Sphere sphere){
    if(!canIntersectAABB(rayPos, rayDir, sphere.aabb)) return false;

    varf4 toSphere = sphere.pos-rayPos;
    varf distanceToSphereSQ = dot(toSphere,toSphere);
    varf rSQ = sphere.radius*sphere.radius;

    if(distanceToSphereSQ < rSQ){
        //the ray started inside the sphere, return no intersect or reverse normals(in wrong method for that)
        return false;
    }

    varf4 dirToSphere = sphere.pos - rayPos;
    varf testA = dot(dirToSphere, rayDir);

    if(testA >= 0.0f){               //sphere is behind rayOriginPos
        varf4 projOfSphereOntoRay = dot(rayDir, dirToSphere)*rayDir+rayPos;
        varf4 toProj = sphere.pos - projOfSphereOntoRay;
        varf distanceToProjSQ = dot(toProj,toProj);

        if(distanceToProjSQ <= rSQ){
            varf4 t = projOfSphereOntoRay - sphere.pos;
            varf lengthtSQ = dot(t,t);
            varf distFromProjCenterToInter1SQ = rSQ - lengthtSQ;

            if(distFromProjCenterToInter1SQ <= rSQ){    //rayPos is outside sphere
                return true;
            }

        }

    }

    return false;
}


varf intersectAxisAlignedBox(varf4 rayPos, varf4 rayDir, AxisAlignedBox aab){
    if(!canIntersectAxisAlignedBox(rayPos, rayDir, aab)) return NAN;           //is this really faster?

    varf tNear = -INFINITY;
    varf tFar = INFINITY;

    //xplanes comp
    if(rayDir.x == 0.0f){  // or comp  EPSILON, -EPSILON ?  //is ray parallel with the xplanes

      if(rayPos.x > aab.upperPos.x){
          return NAN;
      }

      if(rayPos.x < aab.lowerPos.x){
          return NAN;
      }

    }
    else{
        varf planeInterDist1 = (aab.lowerPos.x - rayPos.x)/rayDir.x;
        varf planeInterDist2 = (aab.upperPos.x - rayPos.x)/rayDir.x;

        if(planeInterDist1 > planeInterDist2){
            varf temp = planeInterDist1;
            planeInterDist1 = planeInterDist2;
            planeInterDist2 = temp;
        }

        if(planeInterDist1 > tNear){
            tNear = planeInterDist1;
        }

        if(planeInterDist2 < tFar){
            tFar = planeInterDist2;
        }

        if(tNear > tFar){                   //box missed
          return NAN;
        }

        if(tFar < 0.0f){                    //box is behind ray
          return NAN;
        }

    }

    //yplanes comp
    if(rayDir.y == 0.0f){  // or comp  EPSILON, -EPSILON ?  //is ray parallel with the yplanes

      if(rayPos.y > aab.upperPos.y){
          return NAN;
      }

      if(rayPos.y < aab.lowerPos.y){
          return NAN;
      }

    }
    else{
        varf planeInterDist1 = (aab.lowerPos.y - rayPos.y)/rayDir.y;
        varf planeInterDist2 = (aab.upperPos.y - rayPos.y)/rayDir.y;

        if(planeInterDist1 > planeInterDist2){
            varf temp = planeInterDist1;
            planeInterDist1 = planeInterDist2;
            planeInterDist2 = temp;
        }

        if(planeInterDist1 > tNear){
            tNear = planeInterDist1;
        }

        if(planeInterDist2 < tFar){
            tFar = planeInterDist2;
        }

        if(tNear > tFar){                   //box missed
          return NAN;
        }

        if(tFar < 0.0f){                    //box is behind ray
          return NAN;
        }

    }

    //zplanes comp
    if(rayDir.z == 0.0f){  // or comp  EPSILON, -EPSILON ?  //is ray parallel with the zplanes

      if(rayPos.z > aab.upperPos.z){
          return NAN;
      }

      if(rayPos.z < aab.lowerPos.z){
          return NAN;
      }

    }
    else{
        bool swapped = false;
        varf planeInterDist1 = (aab.lowerPos.z - rayPos.z)/rayDir.z;
        varf planeInterDist2 = (aab.upperPos.z - rayPos.z)/rayDir.z;

        if(planeInterDist1 > planeInterDist2){
            varf temp = planeInterDist1;
            planeInterDist1 = planeInterDist2;
            planeInterDist2 = temp;
        }

        if(planeInterDist1 > tNear){
            tNear = planeInterDist1;
        }

        if(planeInterDist2 < tFar){
            tFar = planeInterDist2;
        }

        if(tNear > tFar){                   //box missed
          return NAN;
        }

        if(tFar < 0.0f){                    //box is behind ray
          return NAN;
        }

    }

//TODO : there has to be a faster method to check if each component of a vector is > or < another vector of the same size and return 1 or 0
//    if((rayPos > aab.lowerPos) && (rayPos < aab.upperPos)){
//    if( (isgreater(rayPos, aab.lowerPos) == 1) && (isless(rayPos,aab.upperPos) == 1) ){
//    if(all(isgreater(rayPos, aab.lowerPos)*INT_MAX) && all(isless(rayPos,aab.upperPos)*INT_MAX)){
//    if(all(isgreater(rayPos, aab.lowerPos)) && all(isless(rayPos,aab.upperPos))){

    if(
        rayPos.x > aab.lowerPos.x &&
        rayPos.x < aab.upperPos.x &&
        rayPos.y > aab.lowerPos.y &&
        rayPos.y < aab.upperPos.y &&
        rayPos.z > aab.lowerPos.z &&
        rayPos.z < aab.upperPos.z
    ){
        //ray starts inside box, return NAN or reverse normals(we are in the wrong function for that)
        return NAN;
    }

    return tNear;
}

Hit intersectGeom(varf4 rayPos, varf4 rayDir){
    Hit hit = (Hit){.distance = INFINITY, .sphereNum = -1, .axisAlignedBoxNum = -1};

    for(int s = 0; s < scene.numSpheres; s++){
        varf distance = intersectSphere(rayPos, rayDir, scene.spheres[s]);

        if(distance < hit.distance){
          hit.distance = distance;
          hit.sphereNum = s;
        }

    }

    for(int b = 0; b < scene.numAxisAlignedBoxes; b++){
        varf distance = intersectAxisAlignedBox(rayPos, rayDir, scene.axisAlignedBoxes[b]);

        if(distance < hit.distance){
          hit.distance = distance;
          hit.axisAlignedBoxNum = b;
          hit.sphereNum = -1;
        }

    }

    if(hit.distance < INFINITY){
        hit.interPos = rayPos + (rayDir*hit.distance);

        if(hit.sphereNum > -1){
            Sphere currSphere = scene.spheres[hit.sphereNum];
            hit.material = currSphere.material;

            if(currSphere.useTransform){
                hit.interNorm = fast_normalize( (hit.interPos - currSphere.pos)*currSphere.inverseScaling );
            }
            else{
                hit.interNorm = fast_normalize(hit.interPos - currSphere.pos);
            }
        }

        if(hit.axisAlignedBoxNum > -1){
            hit.sphereNum = -1;
            AxisAlignedBox box = scene.axisAlignedBoxes[hit.axisAlignedBoxNum];
            hit.material = box.material;

            //TODO : there must be a better way to get an AAB hit normal
            //varf4 boxCenter = ((box.upperPos - box.lowerPos) / 2.0f) + (box.lowerPos);
            //getAxisAlignedBoxNormal(scene.axisAlignedBoxes[hit.axisAlignedBoxNum]);

            
            varf xld = hit.interPos.x - box.lowerPos.x;
            varf yld = hit.interPos.y - box.lowerPos.y;
            varf zld = hit.interPos.z - box.lowerPos.z;
            varf xud = box.upperPos.x - hit.interPos.x;
            varf yud = box.upperPos.y - hit.interPos.y;
            varf zud = box.upperPos.z - hit.interPos.z;

            hit.interNorm = (varf4)(-1.0f,0.0f,0.0f, 0.0f);
            varf lowestValue = xld;

            if(yld < lowestValue){
                hit.interNorm = (varf4)(0.0f,-1.0f,0.0f, 0.0f);
                lowestValue = yld;
            }

            if(zld < lowestValue){
                hit.interNorm = (varf4)(0.0f,0.0f,-1.0f, 0.0f);
                lowestValue = zld;
            }

            if(xud < lowestValue){
                hit.interNorm = (varf4)(1.0f,0.0f,0.0f, 0.0f);
                lowestValue = xud;
            }

            if(yud < lowestValue){
                hit.interNorm = (varf4)(0.0f,1.0f,0.0f, 0.0f);
                lowestValue = yud;
            }

            if(zud < lowestValue){
                hit.interNorm = (varf4)(0.0f,0.0f,1.0f, 0.0f);
                lowestValue = zud;
            }

        }

    }

    return hit;
}


bool canIntersectGeom(varf4 rayPos, varf4 rayDir, varf maxDistanceSQ){

    for(int s = 0; s < scene.numSpheres; s++){
        varf distance = intersectSphere(rayPos, rayDir, scene.spheres[s]);

        if( (distance*distance) < maxDistanceSQ){
            return true;
        }

    }

    for(int b = 0; b < scene.numAxisAlignedBoxes; b++){
        varf distance = intersectAxisAlignedBox(rayPos, rayDir, scene.axisAlignedBoxes[b]);

        if( (distance*distance) < maxDistanceSQ){
            return true;
        }

    }

    return false;
}


varf4 getIntersectColor2(varf4 rayPos, varf4 rayDir, Hit hit, varf r, varf rf, global PointLight *pointLights, int numPointLights){
    varf4 interColor = (varf4)(0.0f,0.0f,0.0f,1.0f);
    varf4 interNorm = hit.interNorm;

    if(hit.material.roughness > 0.0f){
        varf4 epsDir;
        epsDir.x = r;
        r = rand(r);
        epsDir.y = r;
        r = rand(r);
        epsDir.z = r;
        r = rand(r);
        epsDir.w = 0.0f;

        epsDir -= 0.5f;
        epsDir *= hit.material.roughness;

        interNorm = fast_normalize(epsDir + interNorm);
    }

    for(int l = 0; l < numPointLights; l++){
        PointLight light = pointLights[l];
        varf4 shadowRayPos = hit.interPos + (EPSILON * interNorm);
        varf4 dirToLight = light.pos-shadowRayPos;
        varf distToLightSQ = dot(dirToLight,dirToLight);
        dirToLight = fast_normalize(dirToLight);
        bool can = canIntersectGeom(shadowRayPos, dirToLight, distToLightSQ);
        varf shadowProportion = 0.0f; 

        if(can){
            shadowProportion = 1.0f;
        }

        varf incidentLight = 1.0f - shadowProportion;
        varf angleBetweenLightAndNormal = dot(dirToLight, interNorm);

        if(angleBetweenLightAndNormal > 0.0f){
            varf diffuseBrightness = angleBetweenLightAndNormal * hit.material.diffuseFactor * incidentLight;
            interColor += diffuseBrightness * light.color * hit.material.color;            
        }

        varf4 reflectedLightDir = dirToLight - 2.0f * dot(dirToLight, interNorm) * interNorm;
        varf angleBetweenOrigAndReflectedRay = dot(rayDir, reflectedLightDir);

        if(angleBetweenOrigAndReflectedRay > 0.0f){
            varf specularBrightness = native_powr(angleBetweenOrigAndReflectedRay, hit.material.phongSize) * hit.material.phongFactor * incidentLight;
            interColor += specularBrightness * light.color * incidentLight;            
        }

    }

    varf4 ambientColor = hit.material.ambientFactor * hit.material.color;
    interColor += ambientColor;
    return interColor;
}






varf4 getIntersectColor1(varf4 rayPos, varf4 rayDir, Hit hit, varf r, varf rf, global PointLight *pointLights, int numPointLights){
    varf4 interColor = (varf4)(0.0f,0.0f,0.0f,1.0f);
    varf4 interNorm = hit.interNorm;

    if(hit.material.roughness > 0.0f){
        varf4 epsDir;
        epsDir.x = r;
        r = rand(r);
        epsDir.y = r;
        r = rand(r);
        epsDir.z = r;
        r = rand(r);
        epsDir.w = 0.0f;

        epsDir -= 0.5f;
        epsDir *= hit.material.roughness;

        interNorm = fast_normalize(epsDir + interNorm);
    }

    for(int l = 0; l < numPointLights; l++){
        PointLight light = pointLights[l];
        varf4 shadowRayPos = hit.interPos + (EPSILON * interNorm);
        varf4 dirToLight = light.pos-shadowRayPos;
        varf distToLightSQ = dot(dirToLight,dirToLight);
        dirToLight = fast_normalize(dirToLight);
        bool can = canIntersectGeom(shadowRayPos, dirToLight, distToLightSQ);
        varf shadowProportion = 0.0f; 

        if(can){
            shadowProportion = 1.0f;
        }

        varf incidentLight = 1.0f - shadowProportion;
        varf angleBetweenLightAndNormal = dot(dirToLight, interNorm);

        if(angleBetweenLightAndNormal > 0.0f){
            varf diffuseBrightness = angleBetweenLightAndNormal * hit.material.diffuseFactor * incidentLight;
            interColor += diffuseBrightness * light.color * hit.material.color;            
        }

        varf4 reflectedLightDir = dirToLight - 2.0f * dot(dirToLight, interNorm) * interNorm;
        varf angleBetweenOrigAndReflectedRay = dot(rayDir, reflectedLightDir);

        if(angleBetweenOrigAndReflectedRay > 0.0f){
            varf specularBrightness = native_powr(angleBetweenOrigAndReflectedRay, hit.material.phongSize) * hit.material.phongFactor * incidentLight;
            interColor += specularBrightness * light.color * incidentLight;            
        }

    }

    varf4 ambientColor = hit.material.ambientFactor * hit.material.color;
    interColor += ambientColor;

    if(hit.material.reflectivity > 0.0f){
        varf4 reflectedRayDir = rayDir - 2.0f * dot(rayDir, interNorm) * interNorm;
        varf4 reflectedRayPos = hit.interPos + (EPSILON * reflectedRayDir);
        Hit reflectedHit = intersectGeom(reflectedRayPos, reflectedRayDir);

        if(reflectedHit.distance < INFINITY){
            interColor += hit.material.color * hit.material.reflectivity * getIntersectColor2(reflectedRayPos, reflectedRayDir, reflectedHit, r, rf, pointLights, numPointLights);  
        }

    }    

    return interColor;
}





varf4 getIntersectColor(varf4 rayPos, varf4 rayDir, Hit hit, varf r, varf rf, global PointLight *pointLights, int numPointLights){
    varf4 interColor = (varf4)(0.0f,0.0f,0.0f,1.0f);
    varf4 interNorm = hit.interNorm;

    if(hit.material.roughness > 0.0f){
        varf4 epsDir;
        epsDir.x = r;
        r = rand(r);
        epsDir.y = r;
        r = rand(r);
        epsDir.z = r;
        r = rand(r);
        epsDir.w = 0.0f;

        epsDir -= 0.5f;
        epsDir *= hit.material.roughness;

        interNorm = fast_normalize(epsDir + interNorm);
    }

    for(int l = 0; l < numPointLights; l++){
        PointLight light = pointLights[l];
        varf4 shadowRayPos = hit.interPos + (EPSILON * interNorm);
        varf4 dirToLight = light.pos-shadowRayPos;
        varf distToLightSQ = dot(dirToLight,dirToLight);
        dirToLight = fast_normalize(dirToLight);
        bool can = canIntersectGeom(shadowRayPos, dirToLight, distToLightSQ);
        varf shadowProportion = 0.0f; 

        if(can){
            shadowProportion = 1.0f;
        }

        varf incidentLight = 1.0f - shadowProportion;
        varf angleBetweenLightAndNormal = dot(dirToLight, interNorm);

        if(angleBetweenLightAndNormal > 0.0f){
            varf diffuseBrightness = angleBetweenLightAndNormal * hit.material.diffuseFactor * incidentLight;
            interColor += diffuseBrightness * light.color * hit.material.color;            
        }

        varf4 reflectedLightDir = dirToLight - 2.0f * dot(dirToLight, interNorm) * interNorm;
        varf angleBetweenOrigAndReflectedRay = dot(rayDir, reflectedLightDir);

        if(angleBetweenOrigAndReflectedRay > 0.0f){
            varf specularBrightness = native_powr(angleBetweenOrigAndReflectedRay, hit.material.phongSize) * hit.material.phongFactor * incidentLight;
            interColor += specularBrightness * light.color * incidentLight;            
        }

    }

    varf4 ambientColor = hit.material.ambientFactor * hit.material.color;
    interColor += ambientColor;

    if(hit.material.reflectivity > 0.0f){
        varf4 reflectedRayDir = rayDir - 2.0f * dot(rayDir, interNorm) * interNorm;
        varf4 reflectedRayPos = hit.interPos + (EPSILON * reflectedRayDir);
        Hit reflectedHit = intersectGeom(reflectedRayPos, reflectedRayDir);

        if(reflectedHit.distance < INFINITY){
            interColor += hit.material.color * hit.material.reflectivity * getIntersectColor1(reflectedRayPos, reflectedRayDir, reflectedHit, r, rf, pointLights, numPointLights);  
        }

    }    

    return interColor;
}


kernel void progEntry(
        int width,
        int height,
        varf x0,
        varf y0,
        varf rangeX,
        varf rangeY,
        varf frameRand,
        varf frameNum,
        varf cx,
        varf cy,
        varf cz,
        varf lookx,
        varf looky,
        varf lookz,
        varf upx,
        varf upy,
        varf upz,
        varf rightx,
        varf righty,
        varf rightz,
        varf focalLength,
        varf focalJitter,
        varf focalJitterSteps,
        OUTPUT_TYPE output,
        global uint *colorMap,
        int colorMapSize,
        global PointLight *pointLights,
        int numPointLights,
        int maxIterations
) {
    unsigned int ix = get_global_id(0);
    unsigned int iy = get_global_id(1);

    varf discard;
    varf r = fract(native_cos(1.0f*ix)*native_sin(1.0f*iy),&discard);               //randoms change between pixels but will return the same randoms next frame
    varf rf = frameRand + fract(native_cos(1.0f*ix)*native_sin(1.0f*iy),&discard); //randoms change between pixels and from frame to frame
    r = rand(r);
    r = rand(r);
    rf = rand(rf);
    rf = rand(rf);

    varf4 res = (varf4)(1.0f*width, 1.0f*height,  0.0f, 0.0f);
    varf4 camCenter = (varf4)(cx, cy, cz,  0.0f);
    varf4 camUpDir = (varf4)(upx, upy, upz,  0.0f);
    varf4 camLookPos = (varf4)(lookx, looky, lookz,  0.0f);
    varf4 camLookDir = fast_normalize(camLookPos - camCenter);
    varf4 camDim = (varf4)(CAM_WIDTH, (CAM_WIDTH*height)/(1.0f*width),  0.0f, 0.0f);
    varf4 focalPoint = camCenter - camLookDir*focalLength;
    varf4 camRightDir = (varf4)(rightx, righty, rightz,  0.0f);
    varf4 pixelDim = (varf4)(camDim.x/res.x,camDim.y/res.y,0.0f,0.0f);

    varf4 pixel, screenFract, screenFractZeroCentered, planeIntersectPos, rayDir, rayPos, pixelColor;
    varf pixelDetail;
    Hit hit;

    pixel = (varf4)(1.0f*ix, 1.0f*iy,  0.0f, 0.0f);

    varf rx,ry;

#ifdef JITTER_PRIMARY_RAYS
    rx = (rf-0.5f)*0.99f;
    rf = rand(rf);
    ry = (rf-0.5f)*0.99f;
    rf = rand(rf);
    pixel.x += rx;
    pixel.y += ry;
#endif

#ifdef MULTI_SAMPLE

  #ifdef JITTER_MULTI_SAMPLE_RAYS
    varf jmipoffset = 1.499f;
  #else
    varf ipoffset = 0.77f;
    pixel.y -= ipoffset;
  #endif

#endif


    screenFract = (varf4)(pixel.x/res.x,pixel.y/res.y, 0.0f,0.0f);
    screenFractZeroCentered = (varf4)(screenFract.x-0.5f,screenFract.y-0.5f, 0.0f,0.0f);
    planeIntersectPos = camCenter + (camRightDir*screenFractZeroCentered.x*camDim.x) + (camUpDir*screenFractZeroCentered.y*camDim.y);
    rayDir = fast_normalize(planeIntersectPos - focalPoint);
    rayPos = focalPoint;

    pixelColor = (varf4)(0.0f,0.0f,0.0f, 1.0f);
    pixelDetail = 0.0f;

    hit = intersectGeom(rayPos, rayDir);

    if(hit.distance < INFINITY){
        pixelColor = getIntersectColor(rayPos, rayDir, hit, r, rf, pointLights, numPointLights);

#ifdef MULTI_SAMPLE_NEAR_PERPENDICULAR_NORMALS_ONLY        

        if( fabs(dot(rayDir, hit.interNorm)) < 0.10f){
            pixelDetail += 1.0f;
        }

#else
        pixelDetail += 1.0f;
#endif

    }

#ifdef MULTI_SAMPLE

  if(pixelDetail > 0.0f){
    pixel = (varf4)(1.0f*ix, 1.0f*iy,  0.0f, 0.0f);

#ifdef JITTER_MULTI_SAMPLE_RAYS
    rx = (rf-0.5f)*jmipoffset;
    rf = rand(rf);
    ry = (rf-0.5f)*jmipoffset;
    rf = rand(rf);
    pixel.x += rx;
    pixel.y += ry;
#else
    pixel.y += ipoffset;
    pixel.x += ipoffset;
#endif

    screenFract = (varf4)(pixel.x/res.x,pixel.y/res.y, 0.0f,0.0f);
    screenFractZeroCentered = (varf4)(screenFract.x-0.5f,screenFract.y-0.5f, 0.0f,0.0f);
    planeIntersectPos = camCenter + (camRightDir*screenFractZeroCentered.x*camDim.x) + (camUpDir*screenFractZeroCentered.y*camDim.y);
    rayDir = fast_normalize(planeIntersectPos - focalPoint);
    rayPos = focalPoint;

    hit = intersectGeom(rayPos, rayDir);

    if(hit.distance < INFINITY){
        pixelColor += getIntersectColor(rayPos, rayDir, hit, r, rf, pointLights, numPointLights);
    }

    pixel = (varf4)(1.0f*ix, 1.0f*iy,  0.0f, 0.0f);

#ifdef JITTER_MULTI_SAMPLE_RAYS
    rx = (rf-0.5f)*jmipoffset;
    rf = rand(rf);
    ry = (rf-0.5f)*jmipoffset;
    rf = rand(rf);
    pixel.x += rx;
    pixel.y += ry;
#else
    pixel.y += ipoffset;
    pixel.x -= ipoffset;
#endif

    screenFract = (varf4)(pixel.x/res.x,pixel.y/res.y, 0.0f,0.0f);
    screenFractZeroCentered = (varf4)(screenFract.x-0.5f,screenFract.y-0.5f, 0.0f,0.0f);
    planeIntersectPos = camCenter + (camRightDir*screenFractZeroCentered.x*camDim.x) + (camUpDir*screenFractZeroCentered.y*camDim.y);
    rayDir = fast_normalize(planeIntersectPos - focalPoint);
    rayPos = focalPoint;

    hit = intersectGeom(rayPos, rayDir);

    if(hit.distance < INFINITY){
        pixelColor += getIntersectColor(rayPos, rayDir, hit, r, rf, pointLights, numPointLights);
    }

    pixelColor *= 0.33333333f;
  }

#endif

#ifdef FOCAL_BLUR

    for(varf f = 0.0f; f < focalJitterSteps; f+=1.0f){    
        focalLength += (focalJitter/focalJitterSteps);
        focalPoint = camCenter - (camLookDir*focalLength);
        rayDir = fast_normalize(planeIntersectPos - focalPoint);
        rayPos = focalPoint;    

        varf4 pixelColori = (varf4)(0.0f,0.0f,0.0f, 1.0f);

        hit = intersectGeom(rayPos, rayDir);

        if(hit.distance < INFINITY){
            pixelColori = getIntersectColor(rayPos, rayDir, hit, r, rf, pointLights, numPointLights);
        }

        pixelColor += pixelColori;
    }

    pixelColor /= (focalJitterSteps+1.0f);
#endif
    
#ifdef DOUBLE_FP
    write_imagef(output, (int2)(ix,iy), (float4)convert_float4(pixelColor));
#else
    write_imagef(output, (int2)(ix,iy), (float4)pixelColor);
#endif

}