import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.PointerBuffer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opencl.*;
import org.lwjgl.opencl.api.Filter;
import org.lwjgl.opengl.*;
import org.lwjgl.util.Color;
import org.lwjgl.util.ReadableColor;
import org.lwjgl.util.Timer;

import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.FloatBuffer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.*;
import static org.lwjgl.opencl.CL10.*;
import static org.lwjgl.opencl.CL10GL.*;
import static org.lwjgl.opencl.KHRGLEvent.*;
import static org.lwjgl.opengl.AMDDebugOutput.*;
import static org.lwjgl.opengl.ARBCLEvent.*;
import static org.lwjgl.opengl.ARBDebugOutput.*;
import static org.lwjgl.opengl.ARBSync.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL21.*;

/**
 * Computes with OpenCL using multiple GPUs and renders the result with OpenGL.
 * A shared PBO is used as storage for the image.<br/>
 * 'd' toggles between 32/64bit floatingpoint precision<br/>
 *
 */
public class Main {

	// max number of used GPUs
	private static final int MAX_PARALLELISM_LEVEL = 8;

	private static final int COLOR_MAP_SIZE = 32 * 2 * 4;
	private static final int SCENE_MEM_SIZE = 16*4;  //in bytes?, 4 floats which are 4 bytes each?

	private Set<String> params;

	private CLContext clContext;
	private CLCommandQueue[] queues;
	private CLKernel[] kernels;
	private CLProgram[] programs;

	private CLMem[] glBuffers;
	private IntBuffer glIDs;

	private boolean useTextures;

	// Texture rendering
	private int dlist;
	private int vsh;
	private int fsh;
	private int program;

	private CLMem[] colorMap;

	private CLMem[] sceneMem;

	private final PointerBuffer kernel2DGlobalWorkSize;

	// max per pixel iterations to compute the fractal
	private int maxIterations = 500;

	private double frameNum = 0.0;
	private double frameRand = Math.random();

        private boolean vsync = false;
        private boolean fullscreen = false;

	private int width = 1280;
	private int height = 720;

	private double minX = -2;
	private double minY = -1.2;
	private double maxX = 0.6;
	private double maxY = 1.3;

        private double xRot = 0.0; //amount to look left or right, actually rotate camera about its y-axis
        private double xRotFullRotation = Math.PI*2.0;
        private double yRot = 0.0; //amount to look up or down, actually rotate camera about its x-axis
        private double yRotMax = Math.PI*0.5;
        private double yRotMin = -yRotMax;
        private double mouseXTurnAmount = 0.0020;
        private double mouseYTurnAmount = 0.0020;
        private double turnAmount = 0.04;
        private double moveAmount = 0.03;
        private double cx = 0.0;
        private double cy = 0.0;
        private double cz = 0.0;
        private double lookDirx = 0.0;
        private double lookDiry = 0.0;
        private double lookDirz = 1.0;
        private double lookx = 0.0;
        private double looky = 0.0;
        private double lookz = 2.0;
        private double upx = 0.0;
        private double upy = 1.0;
        private double upz = 0.0;
        private double rightx = 1.0;
        private double righty = 0.0;
        private double rightz = 0.0;
        private double focalLength = 0.05;
        private double focalChangeAmount = 0.001;
        private double focalMin = 0.001;
        private double focalMax = 0.20;
        private double focalJitter = 0.0005;
        private double focalJitterSteps = 5.0;
        private double focalJitterChangeAmount = 0.0001;
        private double focalJitterMin = -0.005;
        private double focalJitterMax = 0.005;
        private double focalJitterStepsMax = 10.0;
        private double focalJitterStepsMin = 1.0;

	private boolean dragging;
	private double dragX;
	private double dragY;
	private double dragMinX;
	private double dragMinY;
	private double dragMaxX;
	private double dragMaxY;

	private int mouseX;
	private int mouseY;

	private int slices;

	private boolean drawSeparator;
	private boolean spin = false;
	private boolean doublePrecision = false;
	private boolean focalBlur = false;
	private boolean jitterPrimaryRays = false;
	private boolean multiSample = false;
	private boolean multiSampleNearPerpendicularNormalsOnly = false;
	private boolean jitterMultiSampleRays = false;
	private boolean buffersInitialized;
	private boolean rebuild;

	private boolean run = true;

	// EVENT SYNCING

	private final PointerBuffer syncBuffer = BufferUtils.createPointerBuffer(1);

	private boolean syncGLtoCL; // true if we can make GL wait on events generated from CL queues.
	private CLEvent[] clEvents;
	private GLSync[] clSyncs;

	private boolean syncCLtoGL; // true if we can make CL wait on sync objects generated from GL.
	private GLSync glSync;
	private CLEvent glEvent;

	public Main(final String[] args) {
		params = new HashSet<String>();

		for ( int i = 0; i < args.length; i++ ) {
			final String arg = args[i];

			if ( arg.charAt(0) != '-' && arg.charAt(0) != '/' )
				throw new IllegalArgumentException("Invalid command-line argument: " + args[i]);

			final String param = arg.substring(1);

			if ( "forcePBO".equalsIgnoreCase(param) )
				params.add("forcePBO");
			else if ( "forceCPU".equalsIgnoreCase(param) )
				params.add("forceCPU");
			else if ( "debugGL".equalsIgnoreCase(param) )
				params.add("debugGL");
			else if ( "iterations".equalsIgnoreCase(param) ) {
				if ( args.length < i + 1 + 1 )
					throw new IllegalArgumentException("Invalid iterations argument specified.");

				try {
					this.maxIterations = Integer.parseInt(args[++i]);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Invalid number of iterations specified.");
				}
			} else if ( "res".equalsIgnoreCase(param) ) {
				if ( args.length < i + 2 + 1 )
					throw new IllegalArgumentException("Invalid res argument specified.");

				try {
					this.width = Integer.parseInt(args[++i]);
					this.height = Integer.parseInt(args[++i]);

					if ( width < 1 || height < 1 )
						throw new IllegalArgumentException("Invalid res dimensions specified.");
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Invalid res dimensions specified.");
				}
			}
		}

		kernel2DGlobalWorkSize = BufferUtils.createPointerBuffer(2);
	}

	public static void main(String args[]) {
		Main demo = new Main(args);
		demo.init();
		demo.run();
	}

/**
 * Set the display mode to be used 
 * 
 * @param width The width of the display required
 * @param height The height of the display required
 * @param fullscreen True if we want fullscreen mode
 */
public static boolean SetDisplayMode(int width, int height, boolean fullscreen) {

    // return if requested DisplayMode is already set
    if ((Display.getDisplayMode().getWidth() == width) && 
        (Display.getDisplayMode().getHeight() == height) && 
	(Display.isFullscreen() == fullscreen)) {
	    return true;
    }

    try {
        DisplayMode targetDisplayMode = null;
		
	if (fullscreen) {
	    DisplayMode[] modes = Display.getAvailableDisplayModes();
 	    int freq = 0;
				
	    for (int i=0;i<modes.length;i++) {
	        DisplayMode current = modes[i];
					
		if ((current.getWidth() == width) && (current.getHeight() == height)) {
		    if ((targetDisplayMode == null) || (current.getFrequency() >= freq)) {
		        if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
			    targetDisplayMode = current;
			    freq = targetDisplayMode.getFrequency();
                        }
                    }

		    // if we've found a match for bpp and frequence against the 
		    // original display mode then it's probably best to go for this one
		    // since it's most likely compatible with the monitor
		    if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) &&
                        (current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())) {
                            targetDisplayMode = current;
                            break;
                    }

                }

            }

        } else {
            targetDisplayMode = new DisplayMode(width,height);
        }

        if (targetDisplayMode == null) {
            System.out.println("Failed to find value mode: "+width+"x"+height+" fs="+fullscreen);
            return false;
        }

        Display.setDisplayMode(targetDisplayMode);
        Display.setFullscreen(fullscreen);
    } catch (LWJGLException e) {
        System.out.println("Unable to setup mode "+width+"x"+height+" fullscreen="+fullscreen + e);
        return false;
    }

    return true;
}

	public void init() {
		try {
			CL.create();

			if(!SetDisplayMode(width, height, fullscreen)){
				SetDisplayMode(width, height, false);
                        }

////Display.getAvailableDisplayModes()
//DisplayMode dm = Display.getDesktopDisplayMode();
//Display.setDisplayMode(dm);
//Display.setFullscreen(true);
			//Display.setDisplayMode(new DisplayMode(width, height));
			Display.setTitle("Demo");
			Display.setSwapInterval(0);
			Display.create(new PixelFormat(), new ContextAttribs().withDebug(params.contains("debugGL")));
			//Display.create(new PixelFormat(), new ContextAttribs());
                        Display.setVSyncEnabled(vsync);
                        Keyboard.create();
                        Keyboard.enableRepeatEvents(false);
                        Mouse.create();
			Mouse.setGrabbed(true);
		} catch (LWJGLException e) {
			throw new RuntimeException(e);
		}

		try {
			initCL(Display.getDrawable());
		} catch (Exception e) {
			if ( clContext != null )
				clReleaseContext(clContext);
			Display.destroy();
			throw new RuntimeException(e);
		}

		glDisable(GL_DEPTH_TEST);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		initView(Display.getDisplayMode().getWidth(), Display.getDisplayMode().getHeight());

		initGLObjects();
		glFinish();

		setKernelConstants();
	}

	private void initCL(Drawable drawable) throws Exception {
		// Find a platform
		List<CLPlatform> platforms = CLPlatform.getPlatforms();
		if ( platforms == null )
			throw new RuntimeException("No OpenCL platforms found.");

		final CLPlatform platform = platforms.get(0); // just grab the first one

		// Find devices with GL sharing support
		final Filter<CLDevice> glSharingFilter = new Filter<CLDevice>() {
			public boolean accept(final CLDevice device) {
				final CLDeviceCapabilities caps = CLCapabilities.getDeviceCapabilities(device);
				return caps.CL_KHR_gl_sharing;
			}
		};
		int device_type = params.contains("forceCPU") ? CL_DEVICE_TYPE_CPU : CL_DEVICE_TYPE_GPU;
		List<CLDevice> devices = platform.getDevices(device_type, glSharingFilter);
		if ( devices == null ) {
			device_type = CL_DEVICE_TYPE_CPU;
			devices = platform.getDevices(device_type, glSharingFilter);
			if ( devices == null )
				throw new RuntimeException("No OpenCL devices found with KHR_gl_sharing support.");
		}

		// Create the context
		clContext = CLContext.create(platform, devices, new CLContextCallback() {
			protected void handleMessage(final String errinfo, final ByteBuffer private_info) {
				System.out.println("[CONTEXT MESSAGE] " + errinfo);
			}
		}, drawable, null);

		slices = min(devices.size(), MAX_PARALLELISM_LEVEL);

		// create command queues for every GPU, setup colormap and init kernels
		queues = new CLCommandQueue[slices];
		kernels = new CLKernel[slices];
		colorMap = new CLMem[slices];
		sceneMem = new CLMem[slices];

		for ( int i = 0; i < slices; i++ ) {
			colorMap[i] = clCreateBuffer(clContext, CL_MEM_READ_ONLY, COLOR_MAP_SIZE, null);
			colorMap[i].checkValid();

			sceneMem[i] = clCreateBuffer(clContext, CL_MEM_READ_ONLY, SCENE_MEM_SIZE, null);
			sceneMem[i].checkValid();

			// create command queue and upload color map buffer on each used device
			queues[i] = clCreateCommandQueue(clContext, devices.get(i), CL_QUEUE_PROFILING_ENABLE, null);
			queues[i].checkValid();

			final ByteBuffer colorMapBuffer = clEnqueueMapBuffer(queues[i], colorMap[i], CL_TRUE, CL_MAP_WRITE, 0, COLOR_MAP_SIZE, null, null, null);
			initColorMap(colorMapBuffer.asIntBuffer(), 32, Color.BLUE, Color.GREEN, Color.RED);
			clEnqueueUnmapMemObject(queues[i], colorMap[i], colorMapBuffer, null, null);

			final ByteBuffer sceneMemBuffer = clEnqueueMapBuffer(queues[i], sceneMem[i], CL_TRUE, CL_MAP_WRITE, 0, SCENE_MEM_SIZE, null, null, null);
			initSceneMem(sceneMemBuffer.asFloatBuffer());
			clEnqueueUnmapMemObject(queues[i], sceneMem[i], sceneMemBuffer, null, null);
		}

		// check if we have 64bit FP support on all devices
		// if yes we can use only one program for all devices + one kernel per device.
		// if not we will have to create (at least) one program for 32 and one for 64bit devices.
		// since there are different vendor extensions for double FP we use one program per device.
		// (OpenCL spec is not very clear about this usecases)
		boolean all64bit = true;
		for ( CLDevice device : devices ) {
			if ( !isDoubleFPAvailable(device) ) {
				all64bit = false;
				break;
			}
		}

		// load program(s)
		programs = new CLProgram[all64bit ? 1 : slices];

		final ContextCapabilities caps = GLContext.getCapabilities();

		if ( !caps.OpenGL20 )
			throw new RuntimeException("OpenGL 2.0 is required to run this demo.");
		else if ( device_type == CL_DEVICE_TYPE_CPU && !caps.OpenGL21 )
			throw new RuntimeException("OpenGL 2.1 is required to run this demo.");

		if ( params.contains("debugGL") ) {
			if ( caps.GL_ARB_debug_output )
				glDebugMessageCallbackARB(new ARBDebugOutputCallback());
			else if ( caps.GL_AMD_debug_output )
				glDebugMessageCallbackAMD(new AMDDebugOutputCallback());
		}

		if ( device_type == CL_DEVICE_TYPE_GPU )
			System.out.println("OpenCL Device Type: GPU (Use -forceCPU to use CPU)");
		else
			System.out.println("OpenCL Device Type: CPU");
		for ( int i = 0; i < devices.size(); i++ )
			System.out.println("OpenCL Device #" + (i + 1) + " supports KHR_gl_event = " + CLCapabilities.getDeviceCapabilities(devices.get(i)).CL_KHR_gl_event);

		System.out.println("\nMax Iterations: " + maxIterations + " (Use -iterations <count> to change)");
		System.out.println("Display resolution: " + width + "x" + height + " (Use -res <width> <height> to change)");

		System.out.println("\nOpenGL caps.GL_ARB_sync = " + caps.GL_ARB_sync);
		System.out.println("OpenGL caps.GL_ARB_cl_event = " + caps.GL_ARB_cl_event);

		// Use PBO if we're on a CPU implementation
		useTextures = device_type == CL_DEVICE_TYPE_GPU && (!caps.OpenGL21 || !params.contains("forcePBO"));
		if ( useTextures ) {
			System.out.println("\nCL/GL Sharing method: TEXTURES (use -forcePBO to use PBO + DrawPixels)");
			System.out.println("Rendering method: Shader on a fullscreen quad");
		} else {
			System.out.println("\nCL/GL Sharing method: PIXEL BUFFER OBJECTS");
			System.out.println("Rendering method: DrawPixels");
		}

		buildPrograms();

		// Detect GLtoCL synchronization method
		syncGLtoCL = caps.GL_ARB_cl_event; // GL3.2 or ARB_sync implied
		if ( syncGLtoCL ) {
			clEvents = new CLEvent[slices];
			clSyncs = new GLSync[slices];
			System.out.println("\nGL to CL sync: Using OpenCL events");
		} else
			System.out.println("\nGL to CL sync: Using clFinish");

		// Detect CLtoGL synchronization method
		syncCLtoGL = caps.OpenGL32 || caps.GL_ARB_sync;
		if ( syncCLtoGL ) {
			for ( CLDevice device : devices ) {
				if ( !CLCapabilities.getDeviceCapabilities(device).CL_KHR_gl_event ) {
					syncCLtoGL = false;
					break;
				}
			}
		}
		if ( syncCLtoGL ) {
			System.out.println("CL to GL sync: Using OpenGL sync objects");
		} else
			System.out.println("CL to GL sync: Using glFinish");

		if ( useTextures ) {
			dlist = glGenLists(1);

			glNewList(dlist, GL_COMPILE);
			glBegin(GL_QUADS);
			{
				glTexCoord2f(0.0f, 0.0f);
				glVertex2f(0, 0);

				glTexCoord2f(0.0f, 1.0f);
				glVertex2i(0, height);

				glTexCoord2f(1.0f, 1.0f);
				glVertex2f(width, height);

				glTexCoord2f(1.0f, 0.0f);
				glVertex2f(width, 0);
			}
			glEnd();
			glEndList();

			vsh = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(vsh, getProgramSource("screen.vert"));
			glCompileShader(vsh);

			fsh = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(fsh, getProgramSource("screen.frag"));
			glCompileShader(fsh);

			program = glCreateProgram();
			glAttachShader(program, vsh);
			glAttachShader(program, fsh);
			glLinkProgram(program);

			glUseProgram(program);
			glUniform1i(glGetUniformLocation(program, "progEntry"), 0);
		}

		System.out.println("");
	}

	private void buildPrograms() {
		/*
		 * workaround: The driver keeps using the old binaries for some reason.
		 * to solve this we simple create a new program and release the old.
		 * however rebuilding programs should be possible -> remove when drivers are fixed.
		 * (again: the spec is not very clear about this kind of usages)
		 */
		if ( programs[0] != null ) {
			for ( CLProgram program : programs )
				clReleaseProgram(program);
		}

		try {
			createPrograms();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// disable 64bit floating point math if not available
		for ( int i = 0; i < programs.length; i++ ) {
			final CLDevice device = queues[i].getCLDevice();

			final StringBuilder options = new StringBuilder(useTextures ? "-D USE_TEXTURE" : "");
			final CLDeviceCapabilities caps = CLCapabilities.getDeviceCapabilities(device);
			if ( doublePrecision && isDoubleFPAvailable(device) ) {
				//cl_khr_fp64
				options.append(" -D DOUBLE_FP");

				//amd's verson of double precision floating point math
				if ( !caps.CL_KHR_fp64 && caps.CL_AMD_fp64 )
					options.append(" -D AMD_FP");
			}

			if ( focalBlur ) {
				options.append(" -D FOCAL_BLUR");
			}

			if ( jitterPrimaryRays ) {
				options.append(" -D JITTER_PRIMARY_RAYS");
			}

			if ( multiSample ) {
				options.append(" -D MULTI_SAMPLE");
			}

			if ( multiSampleNearPerpendicularNormalsOnly ) {
				options.append(" -D MULTI_SAMPLE_NEAR_PERPENDICULAR_NORMALS_ONLY");
			}

			if ( jitterMultiSampleRays ) {
				options.append(" -D JITTER_MULTI_SAMPLE_RAYS");
			}

			System.out.println("\nOpenCL COMPILER OPTIONS: " + options);

			try {
				clBuildProgram(programs[i], device, options, null);
			} finally {
				System.out.println("BUILD LOG: " + programs[i].getBuildInfoString(device, CL_PROGRAM_BUILD_LOG));
			}
		}

		rebuild = false;

		// init kernel with constants
		for ( int i = 0; i < kernels.length; i++ )
			kernels[i] = clCreateKernel(programs[min(i, programs.length)], "progEntry", null);
	}

	private void initGLObjects() {
		if ( glBuffers == null ) {
			glBuffers = new CLMem[slices];
			glIDs = BufferUtils.createIntBuffer(slices);
		} else {
			for ( CLMem mem : glBuffers )
				clReleaseMemObject(mem);

			if ( useTextures )
				glDeleteTextures(glIDs);
			else
				glDeleteBuffers(glIDs);
		}

		if ( useTextures ) {
			glGenTextures(glIDs);

			// Init textures
			for ( int i = 0; i < slices; i++ ) {
				glBindTexture(GL_TEXTURE_2D, glIDs.get(i));
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width / slices, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (ByteBuffer)null);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

				glBuffers[i] = clCreateFromGLTexture2D(clContext, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, glIDs.get(i), null);
			}
			glBindTexture(GL_TEXTURE_2D, 0);
		} else {
			glGenBuffers(glIDs);

			// setup one empty PBO per slice
			for ( int i = 0; i < slices; i++ ) {
				glBindBuffer(GL_PIXEL_UNPACK_BUFFER, glIDs.get(i));
				glBufferData(GL_PIXEL_UNPACK_BUFFER, width * height * 4 / slices, GL_STREAM_DRAW);

				glBuffers[i] = clCreateFromGLBuffer(clContext, CL_MEM_WRITE_ONLY, glIDs.get(i), null);
			}
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
		}

		buffersInitialized = true;
	}

	// init kernels with constants

	private void setKernelConstants() {
		for ( int i = 0; i < slices; i++ ) {
			kernels[i]
				.setArg(23, glBuffers[i])
				.setArg(24, colorMap[i])
				.setArg(25, COLOR_MAP_SIZE)
				.setArg(26, sceneMem[i])
				.setArg(27, 2)
				.setArg(28, maxIterations);
		}
	}

	// rendering cycle

	private void run() {
                long endFrameCount = 200l;
		long frameCounter = 0l;
                Timer avgFpsTimer = new Timer();
                Timer frameTimer = new Timer();
		float frameDuration = 0.0f;
		float shortestFrameDuration = Float.MAX_VALUE;
		float longestFrameDuration = 0.0f;

		while ( run ) {
			
			if ( !Display.isVisible() )
				Thread.yield();

			handleIO();
			display();

			Display.update();
			if ( Display.isCloseRequested() )
				break;

			frameCounter++;
                        Timer.tick();
			frameDuration = frameTimer.getTime();

			if(frameDuration < shortestFrameDuration){
				shortestFrameDuration = frameDuration;
			}

			if(frameDuration > longestFrameDuration){
				longestFrameDuration = frameDuration;
			}

			frameTimer.reset();
			
			if(frameCounter == endFrameCount){
                                frameCounter = 0l;
				float duration = avgFpsTimer.getTime();
				System.out.println(endFrameCount + " frames in " + duration + " s");
				System.out.println("average frame time: " + (duration/endFrameCount) + "s = " + (endFrameCount/duration) + "fps");
				System.out.println("shortest frame time: " + shortestFrameDuration + "s = " + (1.0f/shortestFrameDuration) + "fps");
				System.out.println("longest frame time: " + longestFrameDuration + "s = " + (1.0f/longestFrameDuration) + "fps");
				System.out.println();
				shortestFrameDuration = Float.MAX_VALUE;
				longestFrameDuration = 0.0f;
				avgFpsTimer.reset();
			}

		}

		clReleaseContext(clContext);

		if ( useTextures ) {
			glDeleteProgram(program);
			glDeleteShader(fsh);
			glDeleteShader(vsh);

			glDeleteLists(dlist, 1);
		}

		CL.destroy();
		Display.destroy();
	}

	public void display() {
		// TODO: Need to clean-up events, test when ARB_cl_events & KHR_gl_event are implemented.

		// make sure GL does not use our objects before we start computing
		if ( syncCLtoGL && glEvent != null ) {
			for ( final CLCommandQueue queue : queues )
				clEnqueueWaitForEvents(queue, glEvent);
		} else
			glFinish();

		if ( !buffersInitialized ) {
			initGLObjects();
			setKernelConstants();
		}

		if ( rebuild ) {
			buildPrograms();
			setKernelConstants();
		}

		compute(doublePrecision);
		render();
	}

	// OpenCL

	private void compute(final boolean is64bit) {
		frameNum += 1.0;
		frameRand = Math.random();

                if(spin){
                	minX += 0.0005;
                	minY += 0.0005;
                        if(minX >= 1000000.0) minX = -1000000.0;
                        if(minY >= 1000000.0) minY = -1000000.0;
                }

		int sliceWidth = (int)(width / (float)slices);
		double rangeX = (maxX - minX) / slices;
		double rangeY = (maxY - minY);

		kernel2DGlobalWorkSize.put(0, sliceWidth).put(1, height);

		// start computation
		for ( int i = 0; i < slices; i++ ) {
			kernels[i]
				.setArg(0, sliceWidth)
				.setArg(1, height);
			if ( !is64bit || !isDoubleFPAvailable(queues[i].getCLDevice()) ) {
				kernels[i]
					.setArg(2, (float)(minX + rangeX * i))
                                        .setArg(3, (float)minY)
					.setArg(4, (float)rangeX)
                                        .setArg(5, (float)rangeY)
					.setArg(6, (float)frameRand)
					.setArg(7, (float)frameNum)
					.setArg(8, (float)cx)
					.setArg(9, (float)cy)
					.setArg(10, (float)cz)
					.setArg(11, (float)lookx)
					.setArg(12, (float)looky)
					.setArg(13, (float)lookz)
					.setArg(14, (float)upx)
					.setArg(15, (float)upy)
					.setArg(16, (float)upz)
					.setArg(17, (float)rightx)
					.setArg(18, (float)righty)
					.setArg(19, (float)rightz)
					.setArg(20, (float)focalLength)
					.setArg(21, (float)focalJitter)
					.setArg(22, (float)focalJitterSteps);
			} else {
				kernels[i]
					.setArg(2, minX + rangeX * i)
                                        .setArg(3, minY)
					.setArg(4, rangeX)
                                        .setArg(5, rangeY)
					.setArg(6, frameRand)
					.setArg(7, frameNum)
					.setArg(8, cx)
					.setArg(9, cy)
					.setArg(10, cz)
					.setArg(11, lookx)
					.setArg(12, looky)
					.setArg(13, lookz)
					.setArg(14, upx)
					.setArg(15, upy)
					.setArg(16, upz)
					.setArg(17, rightx)
					.setArg(18, righty)
					.setArg(19, rightz)
					.setArg(20, focalLength)
					.setArg(21, focalJitter)
					.setArg(22, focalJitterSteps);
			}

			// acquire GL objects, and enqueue a kernel with a probe from the list
			clEnqueueAcquireGLObjects(queues[i], glBuffers[i], null, null);

			clEnqueueNDRangeKernel(queues[i], kernels[i], 2,
			                       null,
			                       kernel2DGlobalWorkSize,
			                       null,
			                       null, null);

			clEnqueueReleaseGLObjects(queues[i], glBuffers[i], null, syncGLtoCL ? syncBuffer : null);
			if ( syncGLtoCL ) {
				clEvents[i] = queues[i].getCLEvent(syncBuffer.get(0));
				clSyncs[i] = glCreateSyncFromCLeventARB(queues[i].getParent(), clEvents[i], 0);
			}
		}

		// block until done (important: finish before doing further gl work)
		if ( !syncGLtoCL ) {
			for ( int i = 0; i < slices; i++ )
				clFinish(queues[i]);
		}
	}

	// OpenGL

	private void render() {
		glClear(GL_COLOR_BUFFER_BIT);

		if ( syncGLtoCL ) {
			for ( int i = 0; i < slices; i++ )
				glWaitSync(clSyncs[i], 0, 0);
		}

		//draw slices
		int sliceWidth = width / slices;

		if ( useTextures ) {
			for ( int i = 0; i < slices; i++ ) {
				int seperatorOffset = drawSeparator ? i : 0;

				glBindTexture(GL_TEXTURE_2D, glIDs.get(i));
				glCallList(dlist);
			}
		} else {
			for ( int i = 0; i < slices; i++ ) {
				int seperatorOffset = drawSeparator ? i : 0;

				glBindBuffer(GL_PIXEL_UNPACK_BUFFER, glIDs.get(i));
				glRasterPos2i(sliceWidth * i + seperatorOffset, 0);

				glDrawPixels(sliceWidth, height, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			}
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
		}

		if ( syncCLtoGL ) {
			glSync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
			glEvent = clCreateEventFromGLsyncKHR(clContext, glSync, null);
		}

		//draw info text
		/*
		textRenderer.beginRendering(width, height, false);

		textRenderer.draw("device/time/precision", 10, height - 15);

		for ( int i = 0; i < slices; i++ ) {
			CLDevice device = queues[i].getDevice();
			boolean doubleFP = doublePrecision && isDoubleFPAvailable(device);
			CLEvent event = probes.getEvent(i);
			long start = event.getProfilingInfo(START);
			long end = event.getProfilingInfo(END);
			textRenderer.draw(device.getType().toString() + i + " "
			                  + (int)((end - start) / 1000000.0f) + "ms @"
			                  + (doubleFP ? "64bit" : "32bit"), 10, height - (20 + 16 * (slices - i)));
		}

		textRenderer.endRendering();
		*/
	}

	private void handleIO() {
                Display.processMessages(); //gets latest Mouse (and Keyboard?) events from OS
                Mouse.poll();
                Keyboard.poll();
                final double dx = Mouse.getDX()*1.0;
                final double dy = Mouse.getDY()*1.0;
		final double dwheel = Mouse.getDWheel()*0.005;

		xRot += mouseXTurnAmount*dx;
		if(xRot <= -xRotFullRotation) xRot += xRotFullRotation;
		if(xRot >= xRotFullRotation) xRot -= xRotFullRotation;

		yRot -= mouseYTurnAmount*dy;
		yRot = Math.min(yRot, yRotMax);
		yRot = Math.max(yRot, yRotMin);

		if ( dwheel > 0 ) {
			//increase focal length, decrease FOV
//			focalLength += focalChangeAmount;
//			focalLength = Math.min(focalLength, focalMax);
			//increase focal length jitter
			focalJitter += focalJitterChangeAmount;
                        focalJitter = Math.min(focalJitter, focalJitterMax);
		}
		else if ( dwheel < 0 ) {
			//decrease focal length, decrease FOV
//			focalLength -= focalChangeAmount;
//			focalLength = Math.max(focalLength, focalMin);
			//decrease focal length jitter
			focalJitter -= focalJitterChangeAmount;
                        focalJitter = Math.max(focalJitter, focalJitterMin);
		}

		if( Keyboard.isKeyDown(Keyboard.KEY_C)){
			//look up
			yRot += turnAmount;
			yRot = Math.min(yRot, yRotMax);
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_V)){
			//look down
			yRot -= turnAmount;
			yRot = Math.max(yRot, yRotMin);
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_Q)){
			//turn left
			xRot -= turnAmount;
                        if(xRot <= -xRotFullRotation) xRot += xRotFullRotation;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_E)){
			//turn right
			xRot += turnAmount;
                        if(xRot >= xRotFullRotation) xRot -= xRotFullRotation;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_R)){
			//increase focal length, decrease FOV
			focalLength += focalChangeAmount;
                        focalLength = Math.min(focalLength, focalMax);
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_F)){
			//decrease focal length, increase FOV
			focalLength -= focalChangeAmount;
                        focalLength = Math.max(focalLength, focalMin);
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_T)){
			//increase focal length jitter
			focalJitter += focalJitterChangeAmount;
                        focalJitter = Math.min(focalJitter, focalJitterMax);
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_G)){
			//decrease focal length jitter
			focalJitter -= focalJitterChangeAmount;
                        focalJitter = Math.max(focalJitter, focalJitterMin);
                }

		double cosx = Math.cos(xRot);
		double sinx = Math.sin(xRot);
		double cosy = Math.cos(yRot);
		double siny = Math.sin(yRot);

		//look forward vector
		double lookDirtempx = 0.0f;
		double lookDirtempy = 0.0f;
		double lookDirtempz = 1.0f;

		//rotate around the x axis
		lookDiry = lookDirtempy*cosy-lookDirtempz*siny;
		lookDirz = lookDirtempy*siny+lookDirtempz*cosy;
		lookDirx = lookDirtempx;

                //copy after all ops complete
		lookDirtempx = lookDirx;
		lookDirtempy = lookDiry;
		lookDirtempz = lookDirz;

                //rotate around the y axis
		lookDirz = lookDirtempz*cosx-lookDirtempx*sinx;
		lookDirx = lookDirtempz*sinx+lookDirtempx*cosx;
		lookDiry = lookDirtempy;

		//up direction vector
		double uptempx = 0.0f;
		double uptempy = 1.0f;
		double uptempz = 0.0f;

		//rotate around the x axis
		upy = uptempy*cosy-uptempz*siny;
		upz = uptempy*siny+uptempz*cosy;
		upx = uptempx;

                //copy after all ops complete
		uptempx = upx;
		uptempy = upy;
		uptempz = upz;

                //rotate around the y axis
		upz = uptempz*cosx-uptempx*sinx;
		upx = uptempz*sinx+uptempx*cosx;
		upy = uptempy;


		//look forward vector
		double righttempx = 1.0f;
		double righttempy = 0.0f;
		double righttempz = 0.0f;

		//rotate around the x axis
		righty = righttempy*cosy-righttempz*siny;
		rightz = righttempy*siny+righttempz*cosy;
		rightx = righttempx;

                //copy after all ops complete
		righttempx = rightx;
		righttempy = righty;
		righttempz = rightz;

                //rotate around the y axis
		rightz = righttempz*cosx-righttempx*sinx;
		rightx = righttempz*sinx+righttempx*cosx;
		righty = righttempy;


		if( Keyboard.isKeyDown(Keyboard.KEY_W)){
			//move backwards
                        cx += lookDirx*moveAmount;
                        cy += lookDiry*moveAmount;
                        cz += lookDirz*moveAmount;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_S)){
			//move forward
                        cx -= lookDirx*moveAmount;
                        cy -= lookDiry*moveAmount;
                        cz -= lookDirz*moveAmount;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_A)){
			//strafe left
                        cx -= rightx*moveAmount;
                        cy -= righty*moveAmount;
                        cz -= rightz*moveAmount;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_D)){
			//strafe right
                        cx += rightx*moveAmount;
                        cy += righty*moveAmount;
                        cz += rightz*moveAmount;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_X)){
			//move up
                        cx += upx*moveAmount;
                        cy += upy*moveAmount;
                        cz += upz*moveAmount;
                }

		if( Keyboard.isKeyDown(Keyboard.KEY_Z)){
			//move down
                        cx -= upx*moveAmount;
                        cy -= upy*moveAmount;
                        cz -= upz*moveAmount;
                }


		//translate look dir to look at pos in world coord
		lookx = cx + focalLength*lookDirx;
		looky = cy + focalLength*lookDiry;
		lookz = cz + focalLength*lookDirz;


		while ( Keyboard.next() ) {
			if ( Keyboard.getEventKeyState() )
				continue;

			final int key = Keyboard.getEventKey();

			if ( Keyboard.KEY_1 <= key && key <= Keyboard.KEY_8 ) {
				int number = key - Keyboard.KEY_1 + 1;
				slices = min(number, min(queues.length, MAX_PARALLELISM_LEVEL));
				System.out.println("NEW PARALLELISM LEVEL: " + slices);
				buffersInitialized = false;
			} else {
				switch ( Keyboard.getEventKey() ) {
					case Keyboard.KEY_SPACE:
						drawSeparator = !drawSeparator;
						System.out.println("SEPARATOR DRAWING IS NOW: " + (drawSeparator ? "ON" : "OFF"));
						break;
					case Keyboard.KEY_M:
						multiSample = !multiSample;
						System.out.println("MULTI-SAMPLING IS NOW: " + (multiSample ? "ON" : "OFF"));
						rebuild = true;
						break;
					case Keyboard.KEY_N:
						multiSampleNearPerpendicularNormalsOnly = !multiSampleNearPerpendicularNormalsOnly;
						if(multiSampleNearPerpendicularNormalsOnly) multiSample= true;
						System.out.println("MULTI-SAMPLING ONLY OF NEAR PERPENDICULAR NORMALS IS NOW: " + (multiSampleNearPerpendicularNormalsOnly ? "ON" : "OFF"));
						System.out.println("MULTI-SAMPLING IS NOW: " + (multiSample ? "ON" : "OFF"));
						rebuild = true;
						break;
					case Keyboard.KEY_B:
						jitterMultiSampleRays = !jitterMultiSampleRays;
						if(jitterMultiSampleRays) multiSample= true;
						System.out.println("JITTER MULTI-SAMPLE RAYS IS NOW: " + (jitterMultiSampleRays ? "ON" : "OFF"));
						System.out.println("MULTI-SAMPLING IS NOW: " + (multiSample ? "ON" : "OFF"));
						rebuild = true;
						break;
					case Keyboard.KEY_P:
						jitterPrimaryRays = !jitterPrimaryRays;
						System.out.println("JITTER PRIMARY RAYS IS NOW: " + (jitterPrimaryRays ? "ON" : "OFF"));
						rebuild = true;
						break;
					case Keyboard.KEY_L:
						doublePrecision = !doublePrecision;
						System.out.println("DOUBLE PRECISION IS NOW: " + (doublePrecision ? "ON" : "OFF"));
						rebuild = true;
						break;
					case Keyboard.KEY_O:
						focalBlur = !focalBlur;
						System.out.println("FOCAL BLUR IS NOW: " + (focalBlur ? "ON" : "OFF"));
						rebuild = true;
						break;
					case Keyboard.KEY_I:
						spin = !spin;
						break;
					case Keyboard.KEY_HOME:
						minX = -2;
						minY = -1.2;
						maxX = 0.6;
						maxY = 1.3;
						cx = 0.0;
						cy = 0.0;
						cz = 0.0;
                                                xRot = 0.0;
                                                yRot = 0.0;
						lookx = 0.0;
						looky = 0.0;
						lookz = 1.0;
						focalLength = 0.05;
						break;
					case Keyboard.KEY_ESCAPE:
						run = false;
						break;
					case Keyboard.KEY_Y:
						//increase focal jitter number of steps
						focalJitterSteps += 1.0f;
			                        focalJitterSteps = Math.min(focalJitterSteps, focalJitterStepsMax);
						break;
					case Keyboard.KEY_H:
						//decrease focal jitter number of steps
						focalJitterSteps -= 1.0f;
			                        focalJitterSteps = Math.max(focalJitterSteps, focalJitterStepsMin);
						break;
				}
			}
		}
	}

	private static boolean isDoubleFPAvailable(CLDevice device) {
		final CLDeviceCapabilities caps = CLCapabilities.getDeviceCapabilities(device);
		return caps.CL_KHR_fp64 || caps.CL_AMD_fp64;
	}

	private void createPrograms() throws IOException {
		final String source = getProgramSource("prog.cl");
		for ( int i = 0; i < programs.length; i++ )
			programs[i] = clCreateProgramWithSource(clContext, source, null);
	}

	private String getProgramSource(final String file) throws IOException {
		InputStream source = null;
		URL sourceURL = Thread.currentThread().getContextClassLoader().getResource(file);
		if(sourceURL != null) {
			source = sourceURL.openStream();
		}
		if ( source == null ) // dev-mode
			source = new FileInputStream(file);
		final BufferedReader reader = new BufferedReader(new InputStreamReader(source));

		final StringBuilder sb = new StringBuilder();
		String line;
		try {
			while ( (line = reader.readLine()) != null )
				sb.append(line).append("\n");
		} finally {
			source.close();
		}

		return sb.toString();
	}

	private static void initColorMap(IntBuffer colorMap, int stepSize, ReadableColor... colors) {
		for ( int n = 0; n < colors.length - 1; n++ ) {
			ReadableColor color = colors[n];
			int r0 = color.getRed();
			int g0 = color.getGreen();
			int b0 = color.getBlue();

			color = colors[n + 1];
			int r1 = color.getRed();
			int g1 = color.getGreen();
			int b1 = color.getBlue();

			int deltaR = r1 - r0;
			int deltaG = g1 - g0;
			int deltaB = b1 - b0;

			for ( int step = 0; step < stepSize; step++ ) {
				float alpha = (float)step / (stepSize - 1);
				int r = (int)(r0 + alpha * deltaR);
				int g = (int)(g0 + alpha * deltaG);
				int b = (int)(b0 + alpha * deltaB);
				colorMap.put((r << 0) | (g << 8) | (b << 16));
			}
		}
	}

	private static void initView(int width, int height) {
		glViewport(0, 0, width, height);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, width, 0.0, height, 0.0, 1.0);
	}

	private static void initSceneMem(FloatBuffer sceneMemBuffer) {
		float[] fa = {
			0.0f, 3.0f, -1.0f, 0.0f,
			1.0f, 1.0f, 1.0f, 1.0f,

			2.0f, 4.0f, -6.0f, 0.0f,
			1.0f, 0.8f, 0.9f, 1.0f
		};

		sceneMemBuffer.put(fa);
//
//		sceneMemBuffer.put(1.5f);
//		sceneMemBuffer.put(2.9f);
	}

}